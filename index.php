<!DOCTYPE html>
<html>
    <head>
        <title>Sure Web Service</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <h1>Upload de Arquivo MusicXml com Sure</h1>
        <?php
        require_once './plugin/unirest-php/src/Unirest.php';
        if (isset($_REQUEST['action'])) {
            if ($_REQUEST['action'] == 'Upload') {
//            $ch = curl_init();
                $filePath = $_FILES['file']['tmp_name'];
                $fileName = $_FILES['file']['name'];
//            $data = array('name' => 'Foo', 'file' => "@$filePath", 'fileName' => $fileName);
//            curl_setopt($ch, CURLOPT_URL, 'http://localhost:8084/LereMaven/rest/file/upload');
//            curl_setopt($ch, CURLOPT_POST, 1);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//            curl_exec($ch);
//            curl_close($ch);
                $headers = array('Accept' => 'application/json');
                $body = array(
                    'nomeArquivo' => $fileName,
                    'file' => Unirest\Request\Body::file($filePath, 'text/plain')
                );

                $response = Unirest\Request::post('http://localhost:8084/LereMaven/rest/file/upload', $headers, $body);
                echo '<h1>o nivel da musica classificada foi: ' . $response->body->nivel . '</h1>';
            }
        }
        ?>

        <form action="" method="post" enctype="multipart/form-data">

            <p>
                Selecione um Arquivo : <input type="file" name="file"  />
            </p>

            <input type="submit" value="Upload" name="action"/>
        </form>

    </body>
</html>
